# Modified Zabbix Java Gateway

This is a modified Zabbix Java Gateway. The goal of this project is to add some improvements to the Zabbix Java Gateway which is used to retrieve JMX data for the [Zabbix](http://www.zabbix.com) monitoring tool.

Builds are available on the Downloads page.

See the Wiki page for more information.