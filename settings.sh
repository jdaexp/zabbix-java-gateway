# This is a configuration file for Zabbix Java Gateway.
# It is sourced by startup.sh and shutdown.sh scripts.

### Option: zabbix.listenIP
#   IP address to listen on.
#
# Mandatory: no
# Default:
# LISTEN_IP="0.0.0.0"

### Option: zabbix.listenPort
#   Port to listen on.
#
# Mandatory: no
# Range: 1024-32767
# Default:
# LISTEN_PORT=10052

### Option: zabbix.pidFile
#   Name of PID file.
#   If omitted, Zabbix Java Gateway is started as a console application.
#
# Mandatory: no
# Default:
# PID_FILE="/tmp/zabbix_java.pid"

PID_FILE="/tmp/zabbix_java.pid"

### Option: zabbix.startPollers
#   Number of worker threads to start.
#   0 specifies a demand based thread pool will be used.
#
# Mandatory: no
# Range: 0-1000
# Default:
# START_POLLERS=0

### Option: zabbix.zabbixUrl
#   The URL of the zabbix front end server
#   which will be used for API calls.
#
# Mandatory: no
# Default:
# ZABBIX_URL=http://localhost/zabbix

### Option: zabbix.apiUser
#   The API username to use
#
# Mandatory: no
# Default:
# API_USER=admin

### Option: zabbix.apiPassword
#   The API password to use
#
# Mandatory: no
# Default:
# API_PASSWORD=zabbix

### Option: JMX_OPTS
#   The JVM properties for JMX configuration. Usually this will have to include
#   the desired management port for the Java Gateway as well as any SSL options.
#   For testing purposes, it could be enabled without authentication.
#   The port that is specified in these options will also have to be configured
#   on the Zabbix host JMX interface for the Java Gateway.
# Mandatory: no
# JMX_OPTS="-Dcom.sun.management.jmxremote.port=10057 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

### Option: DEBUG_CMD_LINE
#   Flag to print the command line Java string. Setting
#   DEBUG_CMD_LINE to anything will print the command that will be used 
#   to start the Java Gateway.
# Mandatory: no
# DEBUG_CMD_LINE=true

